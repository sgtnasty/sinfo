// The Swift Programming Language
// https://docs.swift.org/swift-book

import ArgumentParser

struct Configs: ParsableCommand {
    @Option var configFilePath: String
}

// Get the command line arguments
let arguments = CommandLine.arguments

if arguments.count > 1 {
    // Loop through the arguments
    for i in 1..<arguments.count {
        print("Argument \(i): \(arguments[i])")
    }
} else {
    print("No arguments provided.")
}

print("Hello, world!")
