# sinfo

Short for system info #serversideswift

## Dependencies

- logging: `swift-log` from [Apple](https://apple.github.io/swift-log/)
- command line argument parsing: ArgumentParser from [Apple](https://github.com/apple/swift-argument-parser)
- config file (TOML): `swift-toml` from [jdfergason](https://github.com/jdfergason/swift-toml)
- ORM: `Fluent` from [Vapor](https://docs.vapor.codes/4.0/fluent/overview/)
